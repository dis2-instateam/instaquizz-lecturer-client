instaquizz-lecturer-client
==========================

Based on angular-js.

## Installation

Clone repository and run `npm install` to install all dependencies (runs `bower install`, too).


## How to use

Run `npm start` and see output at [http://localhost:8000/app/](http://localhost:8000/app/).