angular.module('myApp').directive('main', ['$http', function($http) {
  'use strict';
  function link($scope, $element) {
    $scope.questions = [];
    $scope.presentationQuestions = [];
    $scope.presenting = false;

    $scope.refreshQuestions = function() {
      $http.get('http://localhost:3000/api/lecturer/getAllQuestions').success(function(data) {
        $scope.questions = data;
        for(var i = 0; i < $scope.presentationQuestions.length; i++) {
          var qstn = questionById($scope.presentationQuestions[i].id);
          $scope.presentationQuestions[i] = qstn;
        }
      });      
    };

    $scope.refreshQuestions();

    var questionById = function(id) {
      for(var i = 0; $scope.questions.length; i++) {
        if($scope.questions[i].id === id) {
          return $scope.questions[i];
        }
      }
    };
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/main.html',
    link: link
  };
}]);