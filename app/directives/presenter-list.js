angular.module('myApp').directive('presenterList',  ['$http', function($http) {
  'use strict';

  function link($scope, $element) {
    $scope.askQuestion = function(question) {
      $http.post('http://46.101.187.127/server/api/lecturer/askQuestion', question).success(function(data, code) {
        if(code === 200) {
          $scope.questionActive = true;
        }
      });
    };

    $scope.stopAskingQuestion = function() {
      $http.get('http://46.101.187.127/server/api/lecturer/stopAskingQuestion').success(function(data, code) {
        if(code === 200) {
          $scope.questionActive = false;
          $scope.refreshQuestions();
        }
      });
    };

    $scope.backToCreatorMode = function() {
      $scope.presenting = false;
    };
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/presenter-list.html',
    link: link
  };
}]);