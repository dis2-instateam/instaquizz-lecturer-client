angular.module('myApp').directive('listHeader', ['$http', function($http) {
  'use strict';
  function link($scope, $element) {
    $scope.createQuestion = function() {
      var newQ = newQuestion();

      $http.post('http://localhost:3000/api/lecturer/addQuestion', newQ)
        .success(function() {
          $http.get('http://localhost:3000/api/lecturer/getAllQuestions').success(function(data) {
            $scope.questions = data;

            // Set the newly created question as active
            $scope.activeQuestion = $scope.questions[$scope.questions.length - 1];      

          });
        });
      
    };

    $scope.removeChecked = function() {
      for(var i = 0; i < $scope.questions.length; i++) {
        if($scope.questions[i].checked) {
          var question = $scope.questions[i];
          // If the question is pushed to the server, tell him to delete
          if(question.status !== 'new') {
            $http.post('http://localhost:3000/api/lecturer/removeQuestion', question).success(function(data) {
              if($scope.questions.length === i) {
                $scope.refreshQuestions();
              }
            });

            if($scope.activeQuestion === question) {
              $scope.activeQuestion = undefined;
            }
          }
        }
      }
    };

    $scope.startPresentation = function() {
      $scope.presenting = true;
      // Get a list of all checked questions
      $scope.presentationQuestions = [];
      for(var i = 0; i < $scope.questions.length; i++) {
        if($scope.questions[i].checked) {
          $scope.presentationQuestions.push($scope.questions[i]);
        }
      }
    };

    function newQuestion() {
      return {
        question: 'New question',
        type: 'numerical',
        status: 'new'     
      };
    }
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/list-header.html',
    link: link
  };
}]);