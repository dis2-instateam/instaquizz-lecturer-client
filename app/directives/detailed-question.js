angular.module('myApp').directive('detailedQuestion', ['$http', function($http) {
  'use strict';
  
  function link($scope, $element) {

    $scope.submit = function() {

      // Avoid weird errors by not sending checked questions to server
      $scope.activeQuestion.checked = false;

      if($scope.activeQuestion.status === 'new') {
        $http.post('http://46.101.187.127/server/api/lecturer/addQuestion', $scope.activeQuestion)
        .success(responseHandler);
        return;
      }

      if($scope.activeQuestion.status === 'created') {
        $http.post('http://46.101.187.127/server/api/lecturer/editQuestion', $scope.activeQuestion)
        .success(responseHandler);
        return;
      }
    };

    var responseHandler = function(data, status) {
      $http.get('http://46.101.187.127/server/api/lecturer/getAllQuestions').success(function(data) {
        $scope.questions = data;
        $scope.activeQuestion = undefined;
      });
    };

  }

  return {
    restrict: 'E',
    templateUrl: 'directives/detailed-question.html',
    link: link
  };
}]);