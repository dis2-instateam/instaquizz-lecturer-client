angular.module('myApp').directive('choice', [function() {
  'use strict';
  function link($scope, $element) {
  	if(!$scope.activeQuestion) {
  		return;
  	}

  	if($scope.activeQuestion.typeSpecific === undefined) {
  		$scope.activeQuestion.typeSpecific = {};
  	}
		$scope.activeQuestion.typeSpecific.choices = [ '', '', '', '' ];
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/type-specific/choice.html',
    link: link
  };
}]);