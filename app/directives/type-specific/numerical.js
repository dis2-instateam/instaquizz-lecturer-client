angular.module('myApp').directive('numerical', [function() {
  'use strict';
  function link($scope, $element) {
  	
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/type-specific/numerical.html',
    link: link
  };
}]);