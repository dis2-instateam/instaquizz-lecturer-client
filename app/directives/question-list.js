angular.module('myApp').directive('questionList',  ['$http', function($http) {
  'use strict';
  function link($scope, $element) {

    $scope.questionSelected = function(question) {
      $scope.activeQuestion = question;  
      $scope.editMode = 'Edit question';    
    };
  }

  return {
    restrict: 'E',
    templateUrl: 'directives/question-list.html',
    link: link
  };
}]);