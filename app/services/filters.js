angular.module('myApp').filter('reverse', function() {
  return function(items) {
  	if(items === undefined) {
  		return;
  	}
    return items.slice().reverse();
  };
});